﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Lab_Exam.Models;

namespace Lab_Exam.Controllers
{
    [Authorize]
    public class LoginController : Controller
    {
        // GET: Login

        RationCardTbEntities dbObject = new RationCardTbEntities();
        // GET: Login
        public ActionResult SignIn()
        {
            return View("SignIn");
        }
        public ActionResult AfterSignIn(Person user1, string ReturnUrl)
        {
            if (CheckUser(user1))
            {
                FormsAuthentication.SetAuthCookie(user1.Email, false);
                if (ReturnUrl != null)
                {
                    return Redirect(ReturnUrl);
                }
                else
                {
                    return Redirect("/Home/Index");
                }
            }

            else
            {
                ViewBag.message = "You Are not authenticate to access Index page please login with Admin!!!";
                return View("SignIn");
            }

        }
        public bool CheckUser(Person user)
        {
            Person autheEmp = dbObject.People.Where(e => e.Email == user.Email).FirstOrDefault();
            return (autheEmp.Role == "admin" && autheEmp.Password == user.Password);
        }
        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            return Redirect("/Login/SignIn");
        }
    }
}