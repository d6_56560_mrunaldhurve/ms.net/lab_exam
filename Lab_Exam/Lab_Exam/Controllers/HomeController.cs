﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Lab_Exam.Models;


namespace Lab_Exam.Controllers
{
    public class HomeController : Controller
    {
         RationCardTbEntities dbObject=new RationCardTbEntities();
        // GET: Home
        public ActionResult Index()
        {
            return View("Index", dbObject.People.ToList());
            
        }
        public ActionResult Add()
        {
            return View("Add");

        }
        public ActionResult AfterAdd(Person user)
        {
            dbObject.People.Add(user);
            dbObject.SaveChanges();
            return Redirect("/Home/Index");

        }
    }
}